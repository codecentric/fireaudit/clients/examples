export interface Employee {
  id: string;
  name: string;
}

export interface Order {
  id: string;
  employeeId: string;
  productIds: string[];
}

export interface Product {
  id: string;
  name: string;
  supplierId: string;
  price: number;
  category: 'CAT_A' | 'CAT_B' | 'CAT_C';
}

export interface Supplier {
  id: string;
  name: string;
}

export interface User {
  id: string;
  displayName: string;
}
