import { Component, OnInit } from '@angular/core';
import {EmployeeRepoService} from '../../dal/employee-repo.service';
import {Observable} from 'rxjs';
import {Employee} from '../../dal/model';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss']
})
export class EmployeeListComponent implements OnInit {

  employees$: Observable<Employee[]>;
  selectedEmployee: Employee;
  show = false;

  constructor(private employeeRepoService: EmployeeRepoService) { }

  ngOnInit() {
    this.employees$ = this.employeeRepoService.employees$();
  }

  async delete(id: any) {
    await this.employeeRepoService.delete(id);
  }

  open(employee?: Employee) {
    this.selectedEmployee = employee;
    this.show = true;
  }
}
