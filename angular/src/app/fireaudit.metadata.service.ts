import {Injectable} from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import * as firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class FireauditMetadataService {
  constructor(private auth: AngularFireAuth) {}

  async getMetadata(event: string) {
    return {
      event,
      uid: this.auth.auth.currentUser ? this.auth.auth.currentUser.uid : null, // mandatory
      serverTime: firebase.firestore.FieldValue.serverTimestamp(), // optional, but strongly recommended
      userTime: new Date() // optional, handy when tracking offline usage
    };
  }
}
