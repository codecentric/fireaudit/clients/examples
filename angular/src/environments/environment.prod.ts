export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyCfFntp7FiMIL7z-2o8bNz1TZ_av-893aQ',
    authDomain: 'fireaudit-example-client.firebaseapp.com',
    databaseURL: 'https://fireaudit-example-client.firebaseio.com',
    projectId: 'fireaudit-example-client',
    storageBucket: 'fireaudit-example-client.appspot.com',
    messagingSenderId: '522905368665',
    appId: '1:522905368665:web:50891233c3291a7546d948',
    measurementId: 'G-KWVP2DNKV9'
  }
};
