import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {Supplier} from '../../dal/model';
import {SupplierRepoService} from '../../dal/supplier-repo.service';

@Component({
  selector: 'app-supplier-list',
  templateUrl: './supplier-list.component.html',
  styleUrls: ['./supplier-list.component.scss']
})
export class SupplierListComponent implements OnInit {

  suppliers$: Observable<Supplier[]>;
  selectedSupplier: Supplier;
  show = false;

  constructor(private supplierRepoService: SupplierRepoService) { }

  ngOnInit() {
    this.suppliers$ = this.supplierRepoService.suppliers$();
  }

  async delete(id: any) {
    await this.supplierRepoService.delete(id);
  }

  open(supplier?: Supplier) {
    this.selectedSupplier = supplier;
    this.show = true;
  }
}
