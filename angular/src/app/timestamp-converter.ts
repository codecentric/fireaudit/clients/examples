export function convertTimestampsToDate<T>(data: T, includeMetadata: boolean = false): T {
  if (!data) {
    return data;
  }

  const res: T = {} as any;
  Object.keys(data).forEach(key => {
    const value = data[key];
    if (isIterable(value) && value.map) {
      res[key] = value.map(v => (v && v.toDate ? v.toDate() : v));
    } else {
      res[key] = value && value.toDate ? value.toDate() : value;
    }
  });

  if (includeMetadata) {
    const metadata = (res as any).metadata;
    if (metadata) {
      (res as any).metadata = convertTimestampsToDate(metadata);
    }
  }

  return res;
}

function isIterable(obj) {
  if (obj == null) {
    return false;
  }
  return typeof obj[Symbol.iterator] === 'function';
}
