import * as admin from 'firebase-admin';
import {FireAudit} from '@fireaudit/functions/client/fireaudit';

admin.initializeApp();

exports.fireAuditAcc = new FireAudit().build({
    apiKey: 'API_KEY',
    domain: 'EU',
    levels: 2
});

exports.fireAuditEU = new FireAudit().build({
    apiKey: 'API_KEY',
    domain: 'EU',
    levels: 2
});

exports.fireAuditUS = new FireAudit().build({
    apiKey: 'API_KEY',
    domain: 'US',
    region: 'us-central1',
    levels: 2,
});
