import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Employee} from '../../dal/model';
import {EmployeeRepoService} from '../../dal/employee-repo.service';
import {FormService} from '../../form.service';

@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrls: ['./employee-form.component.scss']
})
export class EmployeeFormComponent implements OnInit {

  @Input()
  employee: Employee;

  @Output()
  done = new EventEmitter<void>();

  formGroup: FormGroup;

  constructor(
    private formBuilder: FormBuilder, private formService: FormService, private employeeRepoService: EmployeeRepoService) {
  }

  ngOnInit() {
    this.formGroup = this.formBuilder.group({
      id: new FormControl({value: this.employee ? this.employee.id : undefined, disabled: true}),
      name: [this.employee ? this.employee.name : undefined, [Validators.required]]
    });
  }

  async submit() {
    if (await this.formService.isValid(this.formGroup)) {
      const employee: Employee = {
        id: this.formGroup.controls.id.value,
        name: this.formGroup.controls.name.value
      };
      await this.employeeRepoService.save(employee);
      this.done.emit();
    }
  }
}
