import {Injectable} from '@angular/core';
import {Product} from './model';
import {BaseRepoService} from './base-repo.service';
import {AngularFirestore} from '@angular/fire/firestore';
import {FireAuditService} from './fire-audit.service';

@Injectable({
  providedIn: 'root'
})
export class ProductRepoService extends BaseRepoService<Product> {
  constructor(firestore: AngularFirestore, fireAuditService: FireAuditService) {
    super(firestore, fireAuditService)
  }

  products$ = () => this.collection$(`products`);
  product$ = (id: string) => this.document$(`products`, id);
  save = (product: Product) => this.upsert(`products`, 'ProductRepoService-Save', product);
  delete = (id: string) => this.del(`products`, 'ProductRepoService-Delete', id);
}
