import {AngularFirestore} from '@angular/fire/firestore';
import {FireAuditService} from './fire-audit.service';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

export class BaseRepoService<T> {

  constructor(private firestore: AngularFirestore, private fireAuditService: FireAuditService) {
  }

  protected collection$(collection: string): Observable<T[]> {
    return this.firestore.collection<T>(collection).valueChanges().pipe(map(arr => arr.filter((entry: any) => !entry.fireAudit.delete)));
  }

  protected document$(collection: string, id: string): Observable<T> {
    return this.firestore.collection(`${collection}/${id}`).doc<T>().valueChanges();
  }

  protected async upsert(collection: string, event: string, data: T | any) {
    if (!data.id) {
      data.id = this.firestore.collection(collection).ref.doc().id;
    }
    await this.firestore.doc(`${collection}/${data.id}`).set({
      ...data,
      fireAudit: this.fireAuditService.fireAudit(event)
    });
  }
  protected async del(collection: string, event: string, id: string) {
    await this.firestore.doc(`${collection}/${id}`).update({
      fireAudit: this.fireAuditService.fireAudit(event, true)
    });
  }
}
