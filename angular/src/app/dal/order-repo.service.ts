import {Injectable} from '@angular/core';
import {Order} from './model';
import {BaseRepoService} from './base-repo.service';
import {AngularFirestore} from '@angular/fire/firestore';
import {FireAuditService} from './fire-audit.service';

@Injectable({
  providedIn: 'root'
})
export class OrderRepoService extends BaseRepoService<Order> {
  constructor(firestore: AngularFirestore, fireAuditService: FireAuditService) {
    super(firestore, fireAuditService)
  }

  orders$ = (employeeId: string) => this.collection$(`employees/${employeeId}/orders`);
  order$ = (employeeId: string, id: string) => this.document$(`employees/${employeeId}/orders`, id);
  save = (order: Order) => this.upsert(`employees/${order.employeeId}/orders`, 'OrderRepoService-Save', order);
  delete = (employeeId: string, id: string) => this.del(`employees/${employeeId}/orders`, 'OrderRepoService-Delete', id);
}
