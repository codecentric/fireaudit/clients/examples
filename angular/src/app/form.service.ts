import {Injectable} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {merge, Observable, of} from 'rxjs';
import {filter, map, take} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FormService {
  constructor() {}

  isValid(formGroup: FormGroup): Promise<boolean> {
    return this.isValid$(formGroup).toPromise();
  }

  isValid$(formGroup: FormGroup): Observable<boolean> {
    formGroup.updateValueAndValidity();
    for (const i in formGroup.controls) {
      if (formGroup.controls[i]) {
        formGroup.controls[i].markAsDirty();
        formGroup.controls[i].updateValueAndValidity();
      }
    }

    return merge(of(formGroup.status), formGroup.statusChanges).pipe(
      filter(status => status !== 'PENDING'),
      take(1),
      map(status => status === 'VALID')
    );
  }

  /**
   * Marks all controls in a form group as dirty
   * @param formGroup - The form group to dirty
   */
  markFormGroupDirty(formGroup: FormGroup) {
    Object.values(formGroup.controls).forEach((control: any) => {
      control.markAsDirty();

      if (control.controls) {
        this.markFormGroupDirty(control);
      }
    });
    return formGroup;
  }
}
