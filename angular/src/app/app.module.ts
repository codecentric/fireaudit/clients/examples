import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {FirebaseUIModule, firebase, firebaseui} from 'firebaseui-angular';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {environment} from '../environments/environment';
import {AngularFireModule} from '@angular/fire';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import { EmployeeFormComponent } from './ui/employee-form/employee-form.component';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatCardModule, MatExpansionModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatSelectModule,
  MatToolbarModule
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { EmployeeListComponent } from './ui/employee-list/employee-list.component';
import {en_US, NgZorroAntdModule, NZ_I18N} from 'ng-zorro-antd';
import { SupplierListComponent } from './ui/supplier-list/supplier-list.component';
import { SupplierFormComponent } from './ui/supplier-form/supplier-form.component';
import { ProductFormComponent } from './ui/product-form/product-form.component';
import { ProductListComponent } from './ui/product-list/product-list.component';
import { OrderFormComponent } from './ui/order-form/order-form.component';
import { OrderListComponent } from './ui/order-list/order-list.component';

const firebaseUiAuthConfig: firebaseui.auth.Config = {
  signInFlow: 'popup',
  signInOptions: [firebase.auth.GoogleAuthProvider.PROVIDER_ID],
  tosUrl: 'https://fireaudit.studio/tos',
  privacyPolicyUrl: 'https://fireaudit.studio/privacy',
  credentialHelper: firebaseui.auth.CredentialHelper.ACCOUNT_CHOOSER_COM
};

@NgModule({
  declarations: [AppComponent, EmployeeFormComponent, EmployeeListComponent, SupplierListComponent, SupplierFormComponent, ProductFormComponent, ProductListComponent, OrderFormComponent, OrderListComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule,
    FirebaseUIModule.forRoot(firebaseUiAuthConfig),

    NgZorroAntdModule,

    MatButtonModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatCardModule,
    MatIconModule,
    MatInputModule,
    MatExpansionModule,
    MatAutocompleteModule,
    MatSelectModule
  ],
  providers: [{ provide: NZ_I18N, useValue: en_US }],
  bootstrap: [AppComponent]
})
export class AppModule {}
