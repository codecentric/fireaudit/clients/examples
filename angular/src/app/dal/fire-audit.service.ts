import {Injectable} from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import * as firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class FireAuditService {

  constructor(private angularFireAuth: AngularFireAuth) {
  }

  fireAudit(event: string, deleted: boolean = false) {
    const uid = this.angularFireAuth.auth.currentUser ? this.angularFireAuth.auth.currentUser.uid : 'NOT_LOGGED_IN';
    return {
      uid,
      delete: deleted,
      event: event || 'Unknown',
      userTime: new Date(),
      serverTime: firebase.firestore.FieldValue.serverTimestamp()
    };
  }
}
