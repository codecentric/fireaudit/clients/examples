import {Injectable} from '@angular/core';
import {Employee} from './model';
import {BaseRepoService} from './base-repo.service';
import {AngularFirestore} from '@angular/fire/firestore';
import {FireAuditService} from './fire-audit.service';

@Injectable({
  providedIn: 'root'
})
export class EmployeeRepoService extends BaseRepoService<Employee> {

  constructor(firestore: AngularFirestore, fireAuditService: FireAuditService) {
    super(firestore, fireAuditService)
  }

  employees$ = () => this.collection$('employees');
  employee$ = (id: string) => this.document$('employees', id);
  save = (employee: Employee) => this.upsert('employees', 'EmployeeRepoService-Save', employee);
  delete = (id: string) => this.del('employees', 'EmployeeRepoService-Delete', id);
}
