import {Injectable} from '@angular/core';
import {Supplier} from './model';
import {BaseRepoService} from './base-repo.service';
import {AngularFirestore} from '@angular/fire/firestore';
import {FireAuditService} from './fire-audit.service';

@Injectable({
  providedIn: 'root'
})
export class SupplierRepoService extends BaseRepoService<Supplier> {
  constructor(firestore: AngularFirestore, fireAuditService: FireAuditService) {
    super(firestore, fireAuditService)
  }

  suppliers$ = () => this.collection$('suppliers');
  supplier$ = (id: string) => this.document$('suppliers', id);
  save = (supplier: Supplier) => this.upsert('suppliers', 'SupplierRepoService-Save', supplier);
  delete = (id: string) => this.del('suppliers', 'SupplierRepoService-Delete', id);
}
