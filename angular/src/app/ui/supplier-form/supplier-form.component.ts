import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {FormService} from '../../form.service';
import {SupplierRepoService} from '../../dal/supplier-repo.service';
import {Supplier} from '../../dal/model';

@Component({
  selector: 'app-supplier-form',
  templateUrl: './supplier-form.component.html',
  styleUrls: ['./supplier-form.component.scss']
})
export class SupplierFormComponent implements OnInit {

  @Input()
  supplier: Supplier;

  @Output()
  done = new EventEmitter<void>();

  formGroup: FormGroup;

  constructor(
    private formBuilder: FormBuilder, private formService: FormService, private supplierRepoService: SupplierRepoService) {
  }

  ngOnInit() {
    this.formGroup = this.formBuilder.group({
      id: new FormControl({value: this.supplier ? this.supplier.id : undefined, disabled: true}),
      name: [this.supplier ? this.supplier.name : undefined, [Validators.required]]
    });
  }

  async submit() {
    if (await this.formService.isValid(this.formGroup)) {
      const supplier: Supplier = {
        id: this.formGroup.controls.id.value,
        name: this.formGroup.controls.name.value
      };
      await this.supplierRepoService.save(supplier);
      this.done.emit();
    }
  }
}
