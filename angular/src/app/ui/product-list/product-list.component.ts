import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {ProductRepoService} from '../../dal/product-repo.service';
import {Employee, Product} from '../../dal/model';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  products$: Observable<Product[]>;
  selectedProduct: Product;
  show = false;

  constructor(private productRepoService: ProductRepoService) { }

  ngOnInit() {
    this.products$ = this.productRepoService.products$();
  }

  async delete(id: any) {
    await this.productRepoService.delete(id);
  }

  open(product?: Product) {
    this.selectedProduct = product;
    this.show = true;
  }
}
