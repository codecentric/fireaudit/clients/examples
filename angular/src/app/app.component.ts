import {Component, OnDestroy, OnInit} from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import {Observable} from 'rxjs';
import * as firebase from 'firebase/app';
import 'firebase/firestore';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  authState$: Observable<firebase.User | null>;
  show = false;

  constructor(
    private angularFireAuth: AngularFireAuth
  ) {}

  ngOnInit(): void {
      this.authState$ = this.angularFireAuth.authState;
  }

  logout() {
    this.angularFireAuth.auth.signOut();
  }
}
