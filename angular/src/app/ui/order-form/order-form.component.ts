import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Employee, Order, Product, Supplier} from '../../dal/model';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {FormService} from '../../form.service';
import {OrderRepoService} from '../../dal/order-repo.service';
import {ProductRepoService} from '../../dal/product-repo.service';
import {EmployeeRepoService} from '../../dal/employee-repo.service';

@Component({
  selector: 'app-order-form',
  templateUrl: './order-form.component.html',
  styleUrls: ['./order-form.component.scss']
})
export class OrderFormComponent implements OnInit {

  @Input()
  order: Order;

  @Output()
  done = new EventEmitter<void>();

  formGroup: FormGroup;

  employee$: Observable<Employee[]>;
  product$: Observable<Product[]>;

  constructor(
    private formBuilder: FormBuilder,
    private formService: FormService,
    private orderRepoService: OrderRepoService,
    private employeeRepoService: EmployeeRepoService,
    private productRepoService: ProductRepoService) {
  }

  ngOnInit() {
    this.employee$ = this.employeeRepoService.employees$();
    this.product$ = this.productRepoService.products$();
    this.formGroup = this.formBuilder.group({
      id: new FormControl({value: this.order ? this.order.id : undefined, disabled: true}),
      employeeId: [this.order ? this.order.employeeId : undefined, [Validators.required]],
      productIds: [this.order ? this.order.productIds : undefined, [Validators.required]]
    });
  }

  async submit() {
    if (await this.formService.isValid(this.formGroup)) {
      const order: Order = {
        id: this.formGroup.controls.id.value,
        employeeId: this.formGroup.controls.employeeId.value,
        productIds: this.formGroup.controls.productIds.value
      };
      await this.orderRepoService.save(order);
      if (this.order && this.order.employeeId !== order.employeeId) {
        await this.orderRepoService.delete(this.order.employeeId, order.id);
      }
      this.done.emit();
    }
  }
}
