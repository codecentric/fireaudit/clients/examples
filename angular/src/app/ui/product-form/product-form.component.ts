import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Product, Supplier} from '../../dal/model';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {FormService} from '../../form.service';
import {ProductRepoService} from '../../dal/product-repo.service';
import {SupplierRepoService} from '../../dal/supplier-repo.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss']
})
export class ProductFormComponent implements OnInit {

  @Input()
  product: Product;

  @Output()
  done = new EventEmitter<void>();

  formGroup: FormGroup;

  suppliers$: Observable<Supplier[]>;

  constructor(
    private formBuilder: FormBuilder, private formService: FormService, private productRepoService: ProductRepoService, private supplierRepoService: SupplierRepoService) {
  }

  ngOnInit() {
    this.suppliers$  = this.supplierRepoService.suppliers$();
    this.formGroup = this.formBuilder.group({
      id: new FormControl({value: this.product ? this.product.id : undefined, disabled: true}),
      name: [this.product ? this.product.name : undefined, [Validators.required]],
      supplierId: [this.product ? this.product.supplierId : undefined, [Validators.required]],
      price: [this.product ? this.product.price : undefined, [Validators.required]],
      category: [this.product ? this.product.category : undefined]
    });
  }

  async submit() {
    if (await this.formService.isValid(this.formGroup)) {
      const product: Product = {
        id: this.formGroup.controls.id.value,
        name: this.formGroup.controls.name.value,
        supplierId: this.formGroup.controls.supplierId.value,
        price: Number(this.formGroup.controls.price.value) || this.formGroup.controls.price.value,
        category: this.formGroup.controls.category.value
      };
      await this.productRepoService.save(product);
      this.done.emit();
    }
  }
}
