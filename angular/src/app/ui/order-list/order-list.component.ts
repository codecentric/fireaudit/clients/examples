import {Component, OnInit} from '@angular/core';
import {merge, Observable, of} from 'rxjs';
import {Employee, Order} from '../../dal/model';
import {OrderRepoService} from '../../dal/order-repo.service';
import {EmployeeRepoService} from '../../dal/employee-repo.service';
import {map, tap} from 'rxjs/operators';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss']
})
export class OrderListComponent implements OnInit {

   employees$WithOrders$: Observable<Observable<{ orders: Order[]; employee: Employee }>[]>;
  selectedOrder: Order;
  show = false;

  constructor(private employeeRepoService: EmployeeRepoService, private orderRepoService: OrderRepoService) {
  }

  ngOnInit() {
    this.employees$WithOrders$ = this.employeeRepoService.employees$().pipe(
      map(employees => employees.map(employee =>
        merge(
          of({employee, orders: []}),
          this.orderRepoService.orders$(employee.id).pipe(
            map(orders => ({employee, orders}))
          )
        )
      ))
    );
  }

  async delete(employeeId, orderId: any) {
    await this.orderRepoService.delete(employeeId, orderId);
  }

  open(order?: Order) {
    this.selectedOrder = order;
    this.show = true;
  }
}
